% Vypustit vsechny liche prvky seznamu
liche(Sez,L):-liche(Sez,1,L).
liche([],C,[]).
liche([H|T],C,[H|V]):-(C mod 2) =:= 0,!,P1 is C+1, liche(T,P1,V).
liche([H|T],C,V):-P1 is C + 1, liche(T,P1,V).

% rozdil tj. opak pruniku
rozdil([],Sez,[]).
rozdil([H|T],Sez,[H|V]):-not(member(H,Sez)),!,rozdil(T,Sez,V).
rozdil([H|T],Sez,V):-rozdil(T,Sez,V).

% vsechny delitele daneho cisla
del(X,Sez):-del(X,1,Sez).
del(X,X,[X]):-write(X),nl.
del(X,D,[D|T]):-X mod D =:= 0,!, write(D),nl, D1 is D + 1, del(X,D1,T).
del(X,D,V):-D1 is D + 1, del(X,D1,V).

% vypiste vsechny 3 pismena slova, ktere lze vytvorit z 5 pismen (a,b,c,d,e)
% tak ze v kazdem jsou minimalne dve samohlasky

vytkni(X,[X|T],T).
vytkni(X,[H|T],[H|V]):-vytkni(X,T,V).

slovo(X,Y,Z,A,B):-Sam = [a,e,i,o,u],Sou = [b,c,d],vytkni(X,Sam,_),vytkni(Y,Sam,_),vytkni(Z,Sam,_),vytkni(A,Sou,_),vytkni(B,Sou,_).

% je 12 zvířat a mají mít dohromady 42 noh ... a pak ještě, ze koz má být víc
% jak ovci a ovcí víc jak slepic (čili 3 druhy zvířat)

c(1).c(2).c(3).c(4).c(5).c(6).c(7).c(8).c(9).

zvire():-c(K),c(O),K > O,c(S),S > O, K * 4 + O * 4 + S * 2 =:= 42,
  write('Koz: ':K),
  write('Ovci: ':O),
  write('Slepic: ':S).

zvire2():-C = [1,2,3,4,5,6,7,8,9],vytkni(K,C,_),vytkni(O,C,_),K > O,vytkni(S,C,_),S > O, K*4 + O*4 +S*2 =:=42,
write('Koz: ':K),
write('Ovci: ':O),
write('Slepic: ':S).

% pocet navzajem ruznych prvku

ruzne(Sez,P):-ruzne(Sez,[],0,P).
ruzne([],Sez,P,P).
ruzne([H|T],Sez,C,P):-not(member(H,Sez)),!,C1 is C + 1,ruzne(T,[H|Sez],C1,P).
ruzne([H|T],Sez,C,P):-ruzne(T,Sez,C,P).

% pocet navzajem stejnych prvku

stejny(Sez,P):-stejny(Sez,[],0,P).
stejny([],Sez,P,P).
stejny([H|T],Sez,C,P):-not(member(H,Sez)),!,stejny(T,[H|Sez],C,P).
stejny([H|T],Sez,C,P):-C1 is C + 1,stejny(T,Sez,C1,P).

% stejne v seznamu (moc nefunguje)
stjpr([],Sez,[]).
stjpr([H|T],Sez,[H|T2]):-member(H,Sez),!,stjpr(T,Sez,T2).
stjpr([H|T],Sez,V):-stjpr(T,Sez,V).

% oddel N prvnich prvku a zbytek do seznamu

oddel(N,[],[]).
oddel(0,Zb,Zb):-!.
oddel(N,[H|T],Zb):-N1 is N - 1, oddel(N1,T,Zb).

% aritmeticky prumer
prum(Sez,Ap):-prum(Sez,0,0,Ap).
prum([],S,P,Ap):-Ap is S / P.
prum([H|T],S,P,Ap):-S1 is S + H, P1 is P + 1,prum(T,S1,P1,Ap).

% pyramida z kostek 1 - 4 - 9 - nemam

osoba(jana,z,30).
osoba(bedriska,z,20).
osoba(andula,z,35).
osoba(petr,m,45).

% najdi nesjtarsi zenu

nejstarsi(X):-osoba(O,z,V),nejstarsi([O],V,X).
nejstarsi(O,V,X):-osoba(O2,z,V2),not(member(O2,O)),V2 > V,!,nejstarsi([O2|O],V2,X).
nejstarsi([H|T],V,H):-!.

% rozpulte seznam tak, ze vzdy dva prvky za sebou v seznamu sectete,
% kdyz pocet prvku je lichy, tak posledni prvek je posledni prvek seznamu,
% pr.[1,2,3,4,5], vypadne vam [3,7,5]

rozp([],[]).
rozp([X],[X]).
rozp([H,H1|T],[V|T1]):-V is H + H1,rozp(T,T1).

% rozdelte seznam na souhlasky a samohlasky
% sousam(Sez,Sam,Sou)

sousam([],[],[]).
sousam([H|T],[H|Sam],Sou):-member(H,[a,e,i,o,u]),!,sousam(T,Sam,Sou).
sousam([H|T],Sam,[H|Sou]):-sousam(T,Sam,Sou).

% zviratka, pavouci (osm nohou), brouci(sest nohou), dohromady 66nohou,
% brouku vic nez pavouku a minimalne jeden pavouk, kolik jich zije v krabicce

brouci(P,B):-c(P),c(B),B > P, P >= 1, (P * 8) + (B * 6) =:= 66.

% predikat zapasy(tym1,tym2,goly1,goly2), vypiste vsechny zapasy, kde nevyhral domaci tym
zapasy(d,h,1,2).
zapasy(a,s,2,2).
zapasy(t,y,3,2).
zapasy(w,a,8,2).
zapasy(x,r,15,9).

tym():-zapasy(D,H,DS,HS),DS < HS,write(D:H:DS:HS),nl,fail.

% kolikrat je prvek x v seznamu

kolik(X,[],0).
kolik(X,[H|T],P):-X = H, P1 is P + 1, kolik(X,T,P1).
kolik(X,[H|T],P):-kolik(X,T,P).

% smaz ze seznamu vsechny prvky mensi nez mez
% a uloy je do noveho seznamu

smazmez([],M,[],[]).
smazmez([H|T],M,Sez,[H|Sm]):- H < M, smazmez(T,M,Sez,Sm).
smazmez([H|T],M,[H|Sez],Sm):- H >= M, smazmez(T,M,Sez,Sm).

% Ctyri auta (Cervene,Modre,Zelene,Zlute) parkuji na ctyrech prakovacich mistech.
% Zlute auto nesmi stat na 1 a 4 miste.

auto(cervene).
auto(modre).
auto(zelene).
auto(zlute).

auta():-
  auto(A), A \= zlute,
  auto(D), D \= A, D \= zlute,
  auto(B), B \= A, B \= D,
  auto(C), C \= A, C \= B, C \= D,
  write(A:B:C:D),nl,fail.

% Vyberte vsechny studenty z databaze, kteri maji lepsi znamku z
% matiky nez z programovani

zkouska(matika,jan,1).
zkouska(programovani,jan,3).
zkouska(matika,petr,2).
zkouska(programovani,petr,1).
zkouska(matika,john,1).
zkouska(programovani,john,3).

znamka():-zkouska(matika,J,MZ),zkouska(programovani,J,PZ),MZ < PZ, write(J),nl,fail.

% vypis seznam po trech radcich
tri([]).
tri([T]):-write(T).
tri([T,S]):-write(T:S).
tri([A,B,C|T]):-write(A:B:C),tri(T).

% Urcete jestli slovo zacina a konci na stejne pismeno (ne cely palindrom,
% napriklad na slovo alena odpovi prolog “ano” a na slovo prdel “ne”)

psp(Slovo):-name(Slovo,[Prvni|T]),posledni(T,Posl),Prvni = Posl.
posledni([Posl],Posl).
posledni([H|T],Posl):-posledni(T,Posl).

psp():-repeat,read(Slovo),(psp(Slovo),write('Ano');write('Ano')),fail.

% Napište program pro vyhledání prostředního prvku v seznamu liché délky

delka([],0).
delka([H|T],D):-delka(T,D1),D is D1 + 1.

nty(1,[H|_],H).
nty(I,[H|T],E):-I>1,I1 is I - 1,nty(I1,T,E).

prost(Sez,V):-delka(Sez,D),D1 is (D // 2) + 1,nty(D1,Sez,V).

% vytkni
vytkni(X,[X|T], T).
vytkni(X,[H|T], [H|T1]):-vytkni(X,T,T1).

vytkniZ(K):-C=[1,2,3,4,5],vytkni(K,C,_).

% Koruny, dvoukoruny, pětikoruny a máme vymyslet všechny kombinace takové,
% aby výsledek byl 24korun.
mince():-C=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],
  vytkni(K,C,_),
  vytkni(D,C,_),
  vytkni(P,C,_),
  K + 2 * D + P * 5 =:= 24,
  write(K:D:P).

% secist dva seznamt
secti([],[],[]).
secti([H|T],[H1|T1],[H2|T2]):-H2 is H + H1, secti(T,T1,T2).

% vypsat kolik vterin zbyva do pulnoci ze zadaneho casu - vteriny(HH,MM,SS,Kolik).

pulnoc(H,M,S,Kolik):-Kolik is (60*24*60) - (H*60*60+M*60+S).

%rozdelit seznam na pul
rozdel([],[],[]).
rozdel([H],[H],[]).
rozdel([H1,H2|T],[H1|T1],[H2|T2]):-rozdel(T,T1,T2).

rozdel(S,S1,S2):-delka(S,P), P1 is P // 2,(P mod 2 =:= 0,N=P1; N is P1 + 1),!,rozdel(S,S1,S2,N).
rozdel(S,[],S,0). rozdel([H|T],[H|S1],S2,N):-N>0,N1 is N-1,rozdel(T,S1,S2,N1).

kolikruznych(Sez,V):-kolikruznych(Sez,[],V).
kolikruznych([],P,0).
kolikruznych([H|T],P,V):-not(member(H,P)),kolikruznych(T,[H|P],V1),V1 is V + 1.
kolikruznych([H|T],P,V):-kolikruznych(T,P,V).

% je seznam serazen sestupne?
isDesc([H|T]):-isDesc(Sez,H).
isDesc([],_).
isDesc([H|T],P):-H1<P,isDesc(T,H).

% odstranit x-te misto v seznamu

odstr([],_,[]).
odstr([H|T],M,[H|T1]):-M \= 0,M1 is M - 1,odstr(T,M1,T1).
odstr([H|T],M,Od):- M = 0,M1 is M - 1, odstr(T,M1,Od).

interpret('Marek','Kulian',1).
interpret('Jarka','Hrnicek',5).
interpret('Mora','Dala',3).
interpret('Sira','Kala',4).
interpret('Issi','Nevim',2).
interpret('Merin','Fristova',6).

top(X):-top(X,0).
top(X,X).
top(X,P):-P1 is P + 1,interpret(M,PR,P1), write(M:PR:P1),top(X,P1).

% smaz prvni vyskyt prvku v seznamu

smaz(X,[],[]).
smaz(X,[X|T],T).
smaz(X,[H|T],[H|T1):-X\=H,smaz(X,T,T1).

smazVse(X,[],[]).
smazVse(X,[X|T],T1):-smazVse(X,T,T1).
smazVse(X,[X|T],[H|T1]):-smazVse(X,T,T1).

% podmnozina(Mnozina,Podmnozina)
podm([],[]).
% vezmi hlavu a dej ji do podmnoziny
podm([H|T],[H|V]) :- podm(T,V).
% vezmi hlavu a nedavej ji do podmnoziny
podm([H|T],V) :- podm(T,V).
